#pragma once

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsLineItem>
#include <QWidget>

#include "Utils.hpp"

class GraphicsScene : public QGraphicsScene {
    Q_OBJECT
public:
    enum DrawMode{ None, Line, Box };

    GraphicsScene(QObject *parent = 0) : QGraphicsScene(parent) {};

    void setDrawMode(DrawMode d) { drawMode_ = d; shapeOpen = false; }

    void mousePressEvent(QGraphicsSceneMouseEvent *e) override;

    void setPreviewImage(const QPixmap& i)
    {
        prevImage = addPixmap(i);
    }

    auto getPreviewImage() { return prevImage; }

    auto getCurrentShape()
    {
        auto p1 = currentShape->mapToItem(prevImage, currentShape->line().p1());
        auto p2 = currentShape->mapToItem(prevImage, currentShape->line().p2());

        std::pair<cv::Vec2d, cv::Vec2d> pair;
        pair.first = {p1.x(), p1.y()};
        pair.second = {p2.x(), p2.y()};

        return pair;
    }

signals:
    void ShapeComplete();

private:
    DrawMode drawMode_{None};
    bool shapeOpen{false};
    QPointF lastClick;
    QGraphicsLineItem *currentShape{nullptr};
    QGraphicsPixmapItem *prevImage{nullptr};
};



