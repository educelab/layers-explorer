//
// Created by Seth on 6/11/17.
//

#include "MainWindow.hpp"
#include "View.hpp"
#include "Utils.hpp"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QGraphicsPixmapItem>
#include <QtCore>

#include <iostream>

MainWindow::MainWindow(QWidget *parent) : QWidget(parent)
{
    scene = new GraphicsScene(this);

    connect(scene, &GraphicsScene::ShapeComplete, this, &MainWindow::OnShapeComplete);

    h1Splitter = new QSplitter;

    QSplitter *vSplitter = new QSplitter;
    vSplitter->setOrientation(Qt::Vertical);
    vSplitter->addWidget(h1Splitter);

    View *view = new View("Render");
    view->view()->setScene(scene);
    h1Splitter->addWidget(view);

    auto infoPanel = new QFrame();

    auto layout2DInfo = new QHBoxLayout;
    label2d = new QLabel("Length (2D): ");
    value2d = new QLabel("0.0 px");
    layout2DInfo->addWidget(label2d);
    layout2DInfo->addWidget(value2d);

    auto layout3DInfo = new QHBoxLayout;
    label3d = new QLabel("Length (3D): ");
    value3d = new QLabel("0.0 px");
    layout3DInfo->addWidget(label3d);
    layout3DInfo->addWidget(value3d);

    auto topLayout = new QVBoxLayout;
    topLayout->addStretch();
    topLayout->addLayout(layout2DInfo);
    topLayout->addLayout(layout3DInfo);
    topLayout->addStretch();

    infoPanel->setLayout(topLayout);

    h1Splitter->addWidget(infoPanel);

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(vSplitter);
    setLayout(layout);

    setWindowTitle(tr("Viewer"));
}

void MainWindow::setPreviewImage(const cv::Mat &image)
{
    auto i = Mat2QImage(image);
    scene->setPreviewImage(QPixmap::fromImage(i));
}

void MainWindow::OnShapeComplete()
{
    auto shape = scene->getCurrentShape();
    auto dist2d = cv::norm(shape.second - shape.first);
    value2d->setText(QString::number(dist2d));

    auto dist3d = ppm_.measureLength(shape.first, shape.second);
    value3d->setText(QString::number(dist3d));
    update();
}