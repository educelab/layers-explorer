#pragma once

#include <QWidget>
#include <QSplitter>
#include <QPointer>
#include <QLabel>
#include <opencv2/core.hpp>
#include <vc/core/types/PerPixelMap.hpp>

#include "GraphicsScene.hpp"

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);

    void setPreviewImage(const cv::Mat& image);
    void setPPM(volcart::PerPixelMap ppm) { ppm_ = std::move(ppm); }

protected slots:
    void OnShapeComplete();

private:
    QPointer<GraphicsScene> scene;
    QPointer<QSplitter> h1Splitter;
    QPointer<QSplitter> h2Splitter;

    QPointer<QLabel> label2d;
    QPointer<QLabel> value2d;
    QPointer<QLabel> label3d;
    QPointer<QLabel> value3d;

    cv::Mat prevImg_;
    volcart::PerPixelMap ppm_;
};



