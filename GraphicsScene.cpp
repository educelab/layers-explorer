//
// Created by Seth on 6/11/17.
//
#include "GraphicsScene.hpp"

static const QPen DEFAULT_PEN(QBrush(Qt::red),10);

void GraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent *e)
{
    if(drawMode_ != DrawMode::None) {
        if(shapeOpen) {
            currentShape = addLine(QLineF(lastClick, e->scenePos()), DEFAULT_PEN);
            shapeOpen = false;
            emit ShapeComplete();
        } else {
            if(currentShape != nullptr) removeItem(currentShape);
            lastClick = e->scenePos();
            shapeOpen = true;
        }
    } else {
        QGraphicsScene::mousePressEvent(e);
    }
}