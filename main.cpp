#include <iostream>

#include <boost/filesystem.hpp>
#include <opencv2/opencv.hpp>
#include <vc/core/types/PerPixelMap.hpp>
#include <vc/core/types/VolumePkg.hpp>
#include <QApplication>

#include "MainWindow.hpp"

namespace fs = boost::filesystem;
namespace vc = volcart;

int main(int argc, char* argv[])
{
    if(argc < 4) {
        std::cerr << "Usage: " << argv[0] << " [volpkg] [volume-id] [input].ppm" << std::endl;
        return EXIT_FAILURE;
    }

    Q_INIT_RESOURCE(images);

    // load the volpkg
    fs::path vpkgPath = argv[1];
    auto vpkg = vc::VolumePkg::New(vpkgPath);

    // load the volume
    auto volume = vpkg->volume(argv[2]);

    // load the ppm
    fs::path ppmPath = argv[3];
    auto ppm = vc::PerPixelMap::ReadPPM(ppmPath);

    // load the preview image
    auto prevPath = ppmPath.root_path() / (ppmPath.stem().string() + "_prev.png");
    auto previewImg = cv::imread(prevPath.string());

    QApplication app(argc, argv);
    app.setAttribute(Qt::AA_DontCreateNativeWidgetSiblings);

    MainWindow window;
    window.setPreviewImage(previewImg);
    window.setPPM(ppm);
    window.show();

    return app.exec();
}