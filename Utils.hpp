#pragma once

#include <QImage>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

static QImage Mat2QImage(const cv::Mat& nSrc)
{
    cv::Mat tmp;
    cvtColor(nSrc, tmp, cv::COLOR_BGR2RGB);
    QImage result(
            static_cast<const uint8_t*>(tmp.data), tmp.cols, tmp.rows, tmp.step,
            QImage::Format_RGB888);
    // enforce deep copy, see documentation of
    // QImage::QImage( const uchar *dta, int width, int height, Format format )
    result.bits();
    return result;
}
